
jQuery.listSelection = function(elem, width, height, row){

    //get content of tabs
    var getContent = function(elem, tab){
        switch (tab) {
            case "all":
                elem.children("li").show();
                break;
                
            case "selected":
                elem.children("li:not([addedid])").hide();
                elem.children("li[addedid]").show();
                break;
                
            case "unselected":
                elem.children("li[addedid]").hide();
                elem.children("li:not([addedid])").show();
                break;
        }
    }
    
    var hiddenCheck = function(obj){
        switch (curTab()) {
            case "all":
                elem.children("li").show();
                break;
                
            case "selected":
                elem.children("li:not([addedid])").hide();
                elem.children("li[addedid]").show();
                break;
                
            case "unselected":
                elem.children("li[addedid]").hide();
                elem.children("li:not([addedid])").show();
                break;
        }
    }
    
    //add to selected items function
    var addToSelected = function(obj){
        if (obj.hasClass("itemselected")) {
            $("#view_selected_count").text(parseInt($("#view_selected_count").text(), 10) - 1);
            obj.parents("li").removeAttr("addedid");
            obj.children('input').attr('checked',false);
         }
        else {
            $("#view_selected_count").text(parseInt($("#view_selected_count").text(), 10) + 1);
            obj.parents("li").attr("addedid", "tester");
            obj.children('input').attr('checked',true);
			 }
        hiddenCheck(obj);
    }
    
    //bind onmouseover && click event on item
    var bindEventsOnItems = function(elem){
        $.each(elem.children("li").children(".list_item"), function(i, obj){
            obj = $(obj);
		   if (obj.children("input:checked").length != 0) {
				addToSelected(obj);
                obj.toggleClass("itemselected");
                obj.parents("li").toggleClass("liselected");
            }
            obj.click(function(){
                addToSelected(obj);
                obj.toggleClass("itemselected");
                obj.parents("li").toggleClass("liselected");
            });
            obj.mouseover(function(){
                obj.addClass("itemover");
            });
            obj.mouseout(function(){
                $(".itemover").removeClass("itemover");
            });
        });
    }
    
    //bind onclick event on filters
    var bindEventsOnTabs = function(elem){
        $.each($("#selections li"), function(i, obj){
            obj = $(obj);
            obj.click(function(){
                $(".view_on").removeClass("view_on");
                obj.addClass("view_on");
                getContent(elem, obj.attr("id").replace("view_", ""));
            });
        });
    }
    
    //create control tabs
    var createTabs = function(elem, width){
        var html = '<div id="filters" style="width:' + (parseInt(width, 10) + 2) + 'px;">' +
        '<ul class="selections" id="selections"><li id="view_all" class="view_on">' +
        '<a onclick="return false;" href="#">View All</a></li><li id="view_selected" class="">' +
        '<a onclick="return false;" href="#">Selected (<strong id="view_selected_count">0</strong>)</a></li>' +
        '<li id="view_unselected" class=""><a onclick="return false;" href="#">Unselected</a></li></ul>' +
        '<div class="clearer"></div></div>';
        elem.before(html);
    }
    
    //wrap elements with div
    var wrapElements = function(elem, width, height, row){
        elem.children("li").wrapInner('<div class="list_item"></div>');
        $(".list_item").css("height", height + "px");
        var newwidth = Math.ceil((parseInt(width, 10)) / parseInt(row, 10)) - 11;
        $(".list_item").css("width", newwidth + "px");
    }
     var curTab = function(){
        return $(".view_on").attr("id").replace("view_", "");
    }
    //main
    if (typeof(elem) != 'object') 
        elem = $(elem);
    elem.css("width", width + "px");
    createTabs(elem, width);
    wrapElements(elem, width, height, row);
    bindEventsOnTabs(elem);
    bindEventsOnItems(elem);
}
