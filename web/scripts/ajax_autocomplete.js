(
function($)
{
    var methods = {
        init: function(url,options)
        {
            return this.each(function(){
                var $this = $(this);
                var defaults = {
                                    delay: 300,
                                    minLength: 1,
                                    position:{my: 'left top', at: 'left bottom', collision: 'none'},
                                    params_included: true,
                                    change_fn: undefined,
                                    store_assoc_fn: undefined
                                };
                settings = $.extend(defaults,options);
                $this.data('settings',
                {
                    params_included: settings.params_included,
                    change_fn: settings.change_fn,
                    store_assoc_fn: settings.store_assoc_fn
                });
                $this.autocomplete(
                {
                    delay: settings.delay,
                    change: settings.change_fn,
                    minLength: settings.minLength,
                    position: settings.position,
                    source:function(request,response)
                    {
                        settings = $this.data('settings');
                        change_fn = settings.change_fn;
                        store_assoc_fn = settings.store_assoc_fn;
                        final_url = url+'?callback=?';
                        if(settings.params_included) final_url = url+'&callback=?';
                        $.getJSON(
                                    final_url,
                                    request,
                                    function(data)
                                    {
                                        var list = [];
                                        var assoc = new Array();

                                        if(store_assoc_fn==undefined)
                                        {
                                            $.each(data, function(key,val){list.push(val);});
                                            response(list);
                                        }
                                        else
                                        {
                                            $.each(data, function(key,val)
                                            {
                                                list.push(val);
                                                assoc[val]=key;
                                            });
                                            response(list);
                                            store_assoc_fn(assoc);
                                        }
                                    });
                    }
                });
            });
        }
    };
    $.fn.ajax_autocomplete = function(method)
    {
        return methods.init.apply( this, arguments );
    };
}
)(jQuery);






