/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config
config.uiColor = '#9FDAFC';
	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'editing',     groups: [ 'find', 'selection' ] },		
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
				
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },{ name: 'others', groups:['ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry']}
		
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	//config.removeButtons = 'Underline,Subscript,Superscript';
    config.removeButtons = 'Source,About';
	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';
//config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
	
	//config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
	//config.toolbar_Full.push({name:'wiris', groups:['ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry']});
        //config.toolbarGroups=[{name:'wiris', groups::['ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry']}];

        config.forceSimpleAmpersand = true;
	config.allowedContent = true;
	config.extraPlugins = 'print';
	config.extraPlugins += (config.extraPlugins.length == 0 ? '' : ',') + 'ckeditor_wiris';
	//config.toolbar_Full.push({ name: 'wiris',     items : [ 'ckeditor_wiris_formulaEditor', 'ckeditor_wiris_formulaEditorChemistry', 'ckeditor_wiris_CAS' ]});
	config.allowedContent = true;
        config.removePlugins = 'elementspath';
	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';
        config.height=80;
   //   config.enterMode = 2; //disabled <p> completely
        config.enterMode = CKEDITOR.ENTER_BR; // pressing the ENTER KEY input <br/>
        config.shiftEnterMode = CKEDITOR.ENTER_P; //pressing the SHIFT + ENTER KEYS input <p>
        config.autoParagraph = false; // stops automatic insertion of <p> on focus
};
