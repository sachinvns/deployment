// Script Functions for validate the Student Entry Form
//  Author: Arun & Ram
// Last modified :12/06/2011


//-------------------Date Validation-------------------------

function y2k(number) { return (number < 1000) ? number + 1900 : number; }

  function isDate () {

    txt_day=document.getElementById('txt_day');
	txt_month=document.getElementById('txt_month');
	txt_yr=document.getElementById('txt_year');
	day=parseInt(document.getElementById('txt_day').value,10);
    month=parseInt(document.getElementById('txt_month').value,10);
	 year=parseInt(document.getElementById('txt_year').value);
    var txt_date=document.getElementById('txt_date');
    

    var today = new Date();
    year = ((!year) ? y2k(today.getYear()):year);
    month = ((!month) ? today.getMonth():month-1);
    if (!day) return false
    var test = new Date(year,month,day);
    if ( (y2k(test.getYear()) == year) &&
         (month == test.getMonth()) &&
         (day == test.getDate()) )
		 {
        // txt_date.value=day+"/"+ (month)+"/"+year;
		Age();
       }
    else
    {
       
        
		txt_day.value="";
		txt_month.value="";
		txt_yr.value="";
        alert('Invalid Date');
		//txt_date.value="";
    }

}
   
   function date_check()
   {
	var day=0;
	var month=0;
	var yr=0;
	txt_day=document.getElementById('txt_day');
	txt_month=document.getElementById('txt_month');
	txt_yr=document.getElementById('txt_year');
	var cur_dt=new Date();
	var cur_yr= parseInt(cur_dt.getFullYear(),10);
	var date=document.getElementById('txt_date');
   if(txt_day.value!="" && txt_day.value!="dd" )
   {
      
   day=parseInt(document.getElementById('txt_day').value,10);
   if((txt_day.value).length==1)
     txt_day.value="0"+day;
	 
	 if(day<1 || day >31)
	{
	
    alert('Enter Valid Date');
	txt_day.value="";
    txt_day.focus();       
   }
   }
   else
   {
   txt_day.value="dd";
   }
   if(txt_month.value!="" && txt_month.value!="mm")
   {
   month=parseInt(document.getElementById('txt_month').value,10);
   if((txt_month.value).length==1)
      txt_month.value="0"+month;
   if(month<1 || month >12)
	{
   alert('Enter Valid Month');  
        txt_month.value=""  ;
		 txt_month.focus();
   }
   
   }
    else
   {
   txt_month.value="mm";
   }
   
   if(txt_yr.value!="" && txt_yr.value!="yyyy")
   {
   yr=parseInt(document.getElementById('txt_year').value);
    // To validate the year which should not be future and less than 16 yrs
   var yr_diff=0;
   yr_diff=cur_yr-yr;
   if(yr_diff<16 || yr_diff>70)  
	{
   alert('Enter Valid Year'); 
   txt_yr.value=""; 
   yr=0;
   txt_yr.focus();       
   } 
   }
   else
   {
   txt_yr.value="yyyy";
   }
 
 
  if(day!=0 && month!=0 && yr!=0)
  {
    isDate();      
	// date.value=""+day+"/"+month+"/"+yr;
   }
   
   
  }

  function txt_water(txt_id)
  {
     if(txt_id.value=="dd" || txt_id.value=="mm" ||txt_id.value=="yyyy") 
	 {
	 txt_id.value="";
	 }	
  }
  
 
//----------------------calculating Age-----------------------------
  
function Age()
{

var bday=parseInt(document.getElementById('txt_day').value);
var bmo=(parseInt(document.getElementById('txt_month').value)-1);
var byr=parseInt(document.getElementById('txt_year').value);
var txt_age=document.getElementById('txt_age');
var byr;
var age;
var now = new Date();
tday=now.getDate();
tmo=(now.getMonth());
tyr=(now.getFullYear());

{
if((tmo > bmo)||(tmo==bmo & tday>=bday))
{age=byr}

else
{age=byr+1}
//alert((tyr-age)+"");
txt_age.value=(tyr-age)+"";
}
}

//--------------------------Validation for Nominee age------------------

function valid_nomineeAge(txt)
{
	var nominee_age=0;
	nominee_age=parseInt(txt.value,10);
	if(nominee_age<18)
	{
		alert('Nominee should greater than 18');
		txt.value='';
		txt.focus();
	}
	
}

//----------------show/hide hostel details-----------------

function scholar()
		{ 
		
		var drpScholar=document.getElementById('drp_host_days');
		var drpRoomType=document.getElementById('txt_hos_type');
		var txtRoomCapacity=document.getElementById('txt_hos_capa');
		var txt_host_name=document.getElementById('txt_hos_name');
		var txt_host_floor=document.getElementById('txt_hos_floor');
		var txt_host_roomno=document.getElementById('txt_hos_room_no');
		var host_displine_date=document.getElementById('txt_host_dis_date');
		var host_note=document.getElementById('txt_hos_note');
		if(drpScholar.value=='DAYSCHOLAR')
		{
		
		  drpRoomType.value=""; 
		  txtRoomCapacity.value="";
		  txt_host_name.value="";
		  txt_host_floor.value="";
		  txt_host_roomno.value="";
		  host_displine_date.value="";
		  host_note.value="";
		}
		
		} 

// To Validate cut-off marks is greater than 300

function validateCutOff(txt)
{
	cut_off=parseFloat(txt.value);
	if(cut_off > 300)
	{
		alert('You should not give more than 300');
		txt.value='';
		txt.focus();
		
	}
}
