package CAMPS.Admin;

import CAMPS.Connect.connect;
import java.io.IOException;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.util.ResourceBundle;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class changePassword extends HttpServlet {

    protected void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession ses = request.getSession(false);
        try (PrintWriter out = response.getWriter()) {
            connect con = new connect();
            con.conn();
             ResourceBundle rb = ResourceBundle.getBundle("CAMPS.Admin.Psalt");
            String oldPassword = request.getParameter("oldPassword");
            String newPassword = request.getParameter("newPassword");
            String confirmPassword = request.getParameter("confirmPassword");
            String userId = ses.getAttribute("userId").toString();
            if (newPassword.equals(confirmPassword)) {
                //con.read("SELECT * FROM admin.user_master WHERE upassword=(SELECT SHA1(SHA1(MD5(CONCAT(salt.pwdSalt,'" + oldPassword + "',salt.pwdSalt)))) AS pwd FROM (SELECT pwdSalt FROM admin.user_master WHERE userId='" + userId + "') salt)");
                con.read("SELECT * FROM admin.user_master um WHERE (um.upassword=SHA1(SHA1(MD5(CONCAT('"+rb.getString("psalt.first")+"',um.pwdSalt,'" + oldPassword + "',um.pwdSalt,'"+rb.getString("psalt.last")+"')))) OR  um.upassword=SHA1(SHA1(MD5(CONCAT('"+rb.getString("nsalt.first")+"',um.pwdSalt,'" + oldPassword + "',um.pwdSalt,'"+rb.getString("nsalt.last")+"')))) ) AND um.userId='" + userId + "'");
                //log("SELECT * FROM admin.user_master WHERE upassword=(SELECT SHA1(SHA1(MD5(CONCAT(salt.pwdSalt,'" + oldPassword + "',salt.pwdSalt)))) AS pwd FROM (SELECT pwdSalt FROM admin.user_master WHERE userId='" + userId + "') salt)");
                if (con.rs.first() && con.rs.getRow() == 1)//check username password exits
                {                     
                        con.update("UPDATE admin.user_master SET pwdSalt=SHA1(MD5(SHA1(RAND()))),upassword=SHA1(SHA1(MD5(CONCAT('"+rb.getString("nsalt.first")+"',pwdSalt,'" + newPassword + "',pwdSalt,'"+rb.getString("nsalt.last")+"')))),LastUpdateTime=NOW(),status=1 WHERE userId='" + userId + "' ");
                        
                            out.print("<div class=\"success\"><img src=\"" + request.getServletContext().getAttribute("hostname").toString() + "/Images/tick.png\">Password Changed Successfuly</div>");
                                               
                   
                } else {
                    out.print("<div class=\"warning\"><img src=\"" + request.getServletContext().getAttribute("hostname").toString() + "/Images/alert.png\">Old Password Incorrect</div>");
                }
            } else {
                out.print("<div class=\"warning\"><img src=\"" + request.getServletContext().getAttribute("hostname").toString() + "/Images/alert.png\">New Password Does Not Match</div>");
            }
            con.conclose();
        } catch (Exception e) {
            PrintStream out = null;
            e.printStackTrace(out);
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {
        //processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
